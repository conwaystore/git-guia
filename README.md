# GUIA RAPIDA PARA EL USUARIO

__Tabla de Contenido__

* [Texto](#texto)
  * [Encabezados](#encabezados)
  * [Estilos](#estilos)
* [Listas](#listas)
  * [Desordenadas](#desordenadas)
  * [Ordenadas](#ordenadas)
  * [Lista de tareas](#listadetareas)
* [Imagenes](#imagenes)
  * [Local](#local)
  * [Remoto](#remoto)
* [Enlaces](#enlaces)
* [Citas](#citas)
* [Codigos](#codigos)
  * [Línea](#línea)
  * [Bloque](#bloque)
* [Tablas](#tablas)


## Texto
### Encabezados
Como introducir titulos o encabezados.

__[Sintaxis]__
```
# h1 
## h2 
### h3 
#### h4 
##### h5 
###### h6
```

### Estilos
Como darle estilo __negrita__ , *cursiva*, __*combinado*__, ~~borrado~~.

__[Sintaxis]__
```
*estilo cursiva 1*
_estilo cursiva 2_

**estilo negrita 1**
__estilo negrita 2__
    ~~borrado~~
 ```
 <br />
 <br />
 
## Listas

Como elaborar listas desordenadas.

__[Sintaxis]__
 ```
* Articulo 1
* Articulo 2
  * Articulo 2a
  * Articulo 2b
```

__[Renderizado]__
* Articulo 1
* Articulo 2
  * Articulo 2a
  * Articulo 2b

Como elaborar listas ordenadas.

__[Sintaxis]__

```
1. Articulo 1
2. Articulo 2
3. Articulo 3
   * Articulo 3a
   * Articulo 3b
```
__[Renderizado]__
1. Articulo 1
2. Articulo 2
3. Articulo 3
   * Articulo 3a
   * Articulo 3b

Como elaborar una lista de tareas

__[Sintaxis]__
```
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item
```

__[Renderizado]__
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item

<br />
<br />

## Imagenes

Como agregar una imagen al documento desde un archivo local.

__[Sintaxis]__

```
!Conway Logo](Conwaysocial.jpg)
```
__[Renderizado]__


![Conway Logo](Conwaysocial.jpg)


Como agregar una imagen desde una dirección remota.

__[Sintaxis]__
```
![SAP Logo](http://blog.mastersdesap.com/files/2015/11/SAP_PI.png)
```
__[Renderizado]__

![SAP Logo](http://blog.mastersdesap.com/files/2015/11/SAP_PI.png)

<br />
<br />

## Enlaces
Agregar un enlace en un documento.

__[Sintaxis]__

```
http://github.com - automatic!
[GitHub](http://github.com)
```
__[Renderizado]__

[Conway](http://conwaystore.com.pa) <br />
[http://conwaystore.com.pa](http://conwaystore.com.pa)

<br />
<br />

## Citas
Como insertar citas en el documento.

__[Sintaxis]__
```
Albert Eisten dijo:

> Hay una fuerza motriz más poderosa que el vapor,
 la electricidad y la energía atómica: la voluntad.

```
__[Renderizado]__

Albert Eisten dijo:

> Hay una fuerza motriz más poderosa que el vapor,
 la electricidad y la energía atómica: la voluntad.

<br />
<br />

## Codigo

### Línea

__[Sintaxis]__
```
`<kbd>`ctrl`</kbd>`
```

__[Renderizado]__

`<kbd>`ctrl`</kbd>`

### Bloque
Como resaltar una sintaxis utilizando comillas.

````
```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```
````

Tambien puede resaltar una sintaxis aplicando cuatro espacios <br />
de sangria al codigo.
```javascript
    function fancyAlert(arg) {
      if(arg) {
        $.facebox({div:'#foo'})
      }
    }
```
<br />
<br />

## Tablas
Como crear tablas.

```
Primera columna | Segunda columna
------------ | -------------
Contenido celda 1| Contenido celda 2
Contenido en la primera columna | Contenido en la segunda columna 
```

Primera columna | Segunda columna
------------ | -------------
Contenido celda 1| Contenido celda 2
Contenido en la primera columna | Contenido en la segunda columna 

