# Trampas para Git

> Git es un sistema de control de versiones para codigos abiertos que facilita las actividades en su ordenador portatil o de escritorio.

<br/>

## Instalar Git
> Github proporciona a los clientes de escritorio, una interfaz grafica para realizar las tareas comunes 
que ofrece un repositorio, adicional permite la edicion en linea de comandos y la actualizacion automatica 
de Git para escenarios avanzados.

### GitHub for Windows  <br/>[https://windows.github.com](https://windows.github.com)

### GitHub for Mac   <br/>[https://mac.github.com](https://mac.github.com)


>Git esta disponible para los sistemas Linux y POSIX en su sitio web oficial
 [Git SCM](http://git-scm.com)

<br/>

## Configuracion de Herramientas
Como configurar la información de los usuarios en el repositorio local.

__`$ git config --global user.name "[name]"`__ <br/>Establece el nombre de usuario.

---
__`$ git config --global user.email "[email address]"`__ <br/>Establece el correo electronico.

---
__`$ git config --global color.ui auto`__ <br/>Permite resaltar las líneas de comando.

---

<br/>

## Crear Repositorios

Crear un nuevo repositorio u obtener uno desde un  URL existente.

__`$ git init "[project-name]"`__<br/> Crear un nuevo repisotorio local con un nombre especifico.

---
__`$ git clone [url]`__<br/> Descarga un proyecto y todo su historial de versiones.

---

<br/>

## Hacer Cambios 
Revisar ediciones y confirmar cambios.

__`$ git status`__<br/> Lista todas las modificaciones que encuentra en un archivo.

---
__`$ git diff`__<br/> Muestra las diferencias o modificaciones que no se encuentra en el staged.

---
__`$ git add [file]`__<br/> Subimos al staged una imagen de todas las modificaciones pendientes en nuestro archivo de control de versiones.

---
__`$ git diff --staged`__<br/> Muestra las diferencias entre el staged y el ultimo archivo de control de versiones.

---
__`$ git reset [file]`__<br/> Saca el archivo del staged, pero mantiene su conenido.

---
__`$ git commit - m "[descriptive message]"`__<br/> Subimos nuestro archivo del staged al repositorio central.

---

<br/>

## Cambios a Grupos
Nombrar una serie de commits y combinaciones de complemento.

__`$ git branch`__<br/> Lista todas las ramas en el repositorio actual.

---
__`$ git branch [branch - name]`__<br/> Crear una nueva rama.

---
__`$ git checkout [branch - name]`__<br/> Cambia a un rama especifica y actualiza el directorio de trabajo.

---
__`$ git merge [branch]`__<br/> Combina el historial de la rama especificada hacia la rama actual.

---

__`$ git branch -d [branch - name]`__<br/>  Combina el historial de una rama especifica en la rama actual 

---

##  Nombres de archivo de re factor 
Reubica y elimina archivos versionados. 

__`$ git rm [file]`__<br/> Elimina el archivo del directorio de trabajo y del staged.

---


__`$ git rm --cached [file]`__<br/> Remueve el archivo del control de versiones, pero lo conserva localmente.

---

__`$ git mv [file-original] [file-renamed]`__<br/> Cambia el nombre del archivo y lo prepara para realizar commit.

---

## Suprimir Seguimiento 
Excluye archivos y rutas temporales.

__`*.log build/temp-*`__

<br/> El archivo denominado .gitignore suprime los versionados accidentales de archivos y rutas que modifican patrones especificos.

---

__`$ git ls-file --other --ignored --exclude-standard`__<br/> Lista todos los archivos ignorados en un proyecto .

---

## Guardar fragmentos
Archiva y restaura los cambios incompletos.

__`$ git stash`__<br/> Almacena temporalmente todos los archivos en los que rastrea modificaciones.

---

__`$ git stash pop`__<br/> Restaura los archivos mas recientes.

---

__`$ git stash list`__<br/> Lista todos los cambios.

---

__`$ git stash drop`__<br/> Descarta el conjunto de cambios más reciente.

---

## Revisar el historial 
Examina e inspecciona la evolución de los archivos del proyecto.

__`$ git log`__<br/> Lista el historial de versiones en la rama actual.

---

__`$ git log --follow [file]`__<br/> Muestra el historial de versiones de un archivo, incluyendo el cambio de nombre.

---

__`$ git diff [first-branch]...[second-branch]`__<br/> Muestra las diferencias de contenido entre dos ramas.

---

__`$ git show [commit]`__<br/> Salta los metadatos y el contenido de un commit especifico.

---

## Rehacer Commits
> Borra errores y crea historial de reemplazo

__`$ git reset [commit]`__

Deshace todos los commits despues de `[commit]`, conservando cambios locales

__`$ git reset --hard [commit]`__

Descarta toda la historia y cambia de regreso a un commit especifico

---

## Sincronizar Cambios
> Registra un marcador de repositorio e intercambia historia de version

__`$ git fetch [marcador]`__

Descarga toda la historia del marcador de repositorio

__`$ git merge [marcador]/[rama]`__

Combina rama de marcador a la rama local

__`$ git push [alias] [rama]`__

Sube todos commits de la rama local

__`$ git pull`__

Descarga historia de marcador e incorpora cambios

---

<h1>GitHub<small> Training</small></h1>
Aprende mas sobre el uso de GitHub y Git. Envia un correo a nuestro equipo de cpacitacion o visita nuestro sitio web, para conocer horario de eventos de capacitación y disponibilidad de clases privadas.
